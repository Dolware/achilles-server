os.loadAPI("os/registry/libs/generic")

local RegistryStructure = {"os/registry", "os/registry/libs"}

local isRegistryBuilt = true

local OS_NAME = "ACHILLES v1.3"

for i=1,#RegistryStructure do
    if not fs.exists(RegistryStructure[i]) then
        isRegistryBuilt = false
    end
end

function printHeader()
    local time = os.time()
    local formattedTime = textutils.formatTime(time, false)

    local updateHeader = "Up To Date!"
    local cl_ver = generic.strSplit(OS_NAME, " ")
    local sv_ver = networking.getServerOSVersion()

    if cl_ver[2] ~= sv_ver then
        updateHeader = "Update Available!"
    end

    print(OS_NAME .. " | " .. formattedTime .. " | " .. updateHeader)
    print(" ")
    shell.run("net")
end

if isRegistryBuilt == false then
    print("CORRUPTED INSTALL")
    sleep(2)
    os.reboot()
else
    if generic.getModem() ~= nil then
        rednet.open(generic.getModem())
    end

    os.loadAPI("os/registry/libs/networking")

    term.clear()
    term.setCursorPos(1, 1)

    printHeader()
end
