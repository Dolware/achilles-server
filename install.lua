local args = { ... }
term.clear()

if #args ~= 1 then
    print("Invalid arguments for ACHILLES registry(PROTOCOL)")
    return
else
    print("Installing ACHILLES...")

    local RegistryStructure = {"os/registry", "os/registry/libs", "os/registry/pr_"}

    for i=1,#RegistryStructure do
        if not fs.exists(RegistryStructure[i]) then
            fs.makeDir(RegistryStructure[i])
        end
    end

    fs.copy("disk/libs/generic.lua", "os/registry/libs/generic")
    fs.copy("disk/libs/base64.lua", "os/registry/libs/base64")
    fs.copy("disk/libs/json.lua", "os/registry/libs/json")
    fs.copy("disk/libs/networking.lua", "os/registry/libs/networking")

    fs.move("os/registry/pr_", "os/registry/pr_" .. args[1])

    sleep(1)

    fs.copy("disk/libs/tar.lua", "tar")
    fs.copy("disk/update.lua", "update")
    fs.copy("disk/achilles-bl.lua", "startup")
    fs.copy("disk/net.lua", "net")

    if fs.exists("startup") then
        print("ACHILLES installed, rebooting...")
        sleep(1)
        os.reboot()
    else
        print("Failed to install ACHILLES, check root dir!")
    end
end