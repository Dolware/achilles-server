function getServerOSVersion()
    local wh = http.get("https://bitbucket.org/Dolware/achilles-server/raw/c10fa949a6904b20185f716bf5285e046608f3ea/server_version.txt")
    local ver = "UNKNOWN_VERSION"
    if wh then
        ver = wh.readAll()
    else
        ver = "UNKNOWN_VERSION"
    end

    return ver
end